package com.example.SharingBooks.model.constants;

public enum CustomHTTPStatus {;
    public static final int SUCCESSFUL_LOGIN = 200;
    public static final int USER_SAVED = 201;
    public static final int USER_FOUND = 202;
    public static final int RETURN_ALL_USERS = 203;
    public static final int BOOK_SAVED = 204;
    public static final int RETURN_ALL_BOOKS = 205;
    public static final int RETURN_MY_BOOKS = 206;
    public static final int DELETION_SUCCESS = 207;
    public static final int ERROR = 402;
    public static final int EMAIL_EXISTS = 403;
    public static final int EMAIL_NOT_FOUND = 404;
    public static final int USER_PASSWORD_MISMATCH = 405;
    public static final int BOOK_ALREADY_EXIST = 406;
    public static final int BOOK_NOT_FOUND = 407;

};
