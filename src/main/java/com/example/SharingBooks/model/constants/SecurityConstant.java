package com.example.SharingBooks.model.constants;

public class SecurityConstant {
    public static final String SECRET = "FATPLAYER";
    public static final long EXPIRATION_TIME = 10*24*60*60*1000 ; // a mins -> a*60*1000
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/api/v1/registration";
    public static final String LOGIN_URL = "/api/v1/login";
    public static final String GET_USERS_URL = "/api/v1/users";
}
