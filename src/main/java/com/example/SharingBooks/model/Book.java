package com.example.SharingBooks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "books")
public class Book {

    public enum TradeType {
        OWNED,
        WISHLIST,
        TRADABLE,
    }

    @Column(name = "name")
    private String name;

    @Column(name = "category")
    private String category;

    @Column(name = "author")
    private String author;

    @Column(name = "publisher")
    private String publisher;

    @Column(name = "dop")
    private String dop;

    @Column(name = "currentOwner")
    private String currentOwner;

    @Column(name = "tradeType")
    private TradeType tradeType;

    public Book() {
    }

    public Book(String name, String category, String author, String publisher, String dateOfPublished, String currentOwner, TradeType tradeType) {
        this.name = name;
        this.category = category;
        this.author = author;
        this.publisher = publisher;
        this.dop = dateOfPublished;
        this.currentOwner = currentOwner;
        this.tradeType = tradeType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getDop() {
        return dop;
    }

    public void setDop(String dop) {
        this.dop = dop;
    }

    public String getCurrentOwner() {
        return currentOwner;
    }

    public void setCurrentOwner(String currentOwner) {
        this.currentOwner = currentOwner;
    }

    public TradeType getTradeType() {
        return tradeType;
    }

    public void setTradeType(TradeType tradeType) {
        this.tradeType = tradeType;
    }
}
