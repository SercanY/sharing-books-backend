package com.example.SharingBooks.model.dto;

public class BookDto {

    public enum TradeType {
        OWNED,
        WISHLIST,
        TRADEABLE
    }

    private String name;
    private String category;
    private String author;
    private String publisher;
    private String dop;
    private String currentOwnersEmail;
    private TradeType tradeType;

    public static class Builder {
        private String name;
        private String category;
        private String author;
        private String publisher;
        private String dop;
        private String currentOwnersEmail;
        private TradeType tradeType;

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder category(String category) {
            this.category = category;
            return this;

        }

        public Builder author(String author) {
            this.author = author;
            return this;
        }

        public Builder publisher(String publisher) {
            this.publisher = publisher;
            return this;
        }

        public Builder dop(String dop) {
            this.dop = dop;
            return this;
        }

        public Builder currentOwnersEmail(String email) {
            this.currentOwnersEmail = email;
            return this;
        }

        public Builder tradeType(TradeType tradeType) {
            this.tradeType = tradeType;
            return this;
        }

        public BookDto build() {
            BookDto bookDto = new BookDto();
            bookDto.name = this.name;
            bookDto.category = this.category;
            bookDto.author = this.author;
            bookDto.publisher = this.publisher;
            bookDto.dop = this.dop;
            bookDto.currentOwnersEmail = this.currentOwnersEmail;
            bookDto.tradeType = this.tradeType;
            return bookDto;
        }


    }

    private BookDto() {
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public String getAuthor() {
        return author;
    }

    public String getPublisher() {
        return publisher;
    }

    public String getDop() {
        return dop;
    }

    public String getCurrentOwnersEmail() {
        return currentOwnersEmail;
    }

    public TradeType getTradeType() {
        return tradeType;
    }
}
