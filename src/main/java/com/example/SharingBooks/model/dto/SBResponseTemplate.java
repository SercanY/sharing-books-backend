package com.example.SharingBooks.model.dto;

public class SBResponseTemplate<T> {
    private int code;
    private String message;
    private T body;

    public static class Builder<T> {
        private int code;
        private String message;
        private T body;

        public Builder<T> code(int code) {
            this.code = code;
            return this;
        }

        public Builder<T> message(String message) {
            this.message = message;
            return this;
        }

        public Builder<T> body(T body) {
            this.body = body;
            return this;
        }

        public SBResponseTemplate<T> build() {
            SBResponseTemplate<T> sb = new SBResponseTemplate<>();
            sb.code = this.code;
            sb.message = this.message;
            sb.body = this.body;

            return sb;
        }
    }

	private SBResponseTemplate(){}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}

}