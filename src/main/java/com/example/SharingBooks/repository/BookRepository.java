package com.example.SharingBooks.repository;

import com.example.SharingBooks.model.Book;
import com.example.SharingBooks.model.dto.BookDto;
import com.example.SharingBooks.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@SuppressWarnings("ALL")
@Component
public class BookRepository {

    public List<BookDto> getAllBooks(){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        String qry = "from Book";
        Query query = session.createQuery(qry);
        List<BookDto> books = (List<BookDto>)query.list();

        return books;
    }

    public List<BookDto> getMyBooks(String email){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        Query query = session.createQuery("from Book where email = :email");
        query.setParameter("email", email);

        List<BookDto> books = (List<BookDto>)query.list();

        return books;
    }

    public void addBook(BookDto bookDto){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        session.save(bookDto);
        session.getTransaction().commit();
        session.close();
    }

    public void deleteBook(BookDto bookDto){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        BookDto willDeletedBook = session.load(BookDto.class,bookDto.getName());
        session.delete(willDeletedBook);
        session.getTransaction().commit();
        session.close();
    }

    public BookDto searchBook(String name){
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        BookDto Book = session.load(BookDto.class,name);
        return Book;
    }


}
