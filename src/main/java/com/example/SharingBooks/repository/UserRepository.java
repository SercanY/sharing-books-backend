package com.example.SharingBooks.repository;

import com.example.SharingBooks.model.User;
import com.example.SharingBooks.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@SuppressWarnings("ALL")
@Component
public class UserRepository{
	
	public List<User> getAllUser(){
		Session session = HibernateUtil.getSessionFactory().openSession();
	    session.beginTransaction();
	     
	    String qry = "from User";

		Query query = session.createQuery(qry);

		List<User> users = (List<User>)query.list();
	     
	    return users;
	}
	
	public void addUser(User user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
	    session.beginTransaction();
	    
	    session.save(user);
	    session.getTransaction().commit();
	    session.close();
	}
	

	public User getUserByEmail(String email) {
		
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		

		Query query = session.createQuery("from User where email = :email");
		query.setParameter("email", email);
		
		List<User> users = (List<User>)query.list();
		if(users.isEmpty()) {
			return null;
		}
		return users.get(0);
	}
}
