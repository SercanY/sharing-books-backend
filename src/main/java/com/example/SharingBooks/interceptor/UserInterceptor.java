package com.example.SharingBooks.interceptor;

import com.example.SharingBooks.security.JWTUtil;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class UserInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("prehandle");

        // get cookie
        Cookie[] cookieList = request.getCookies();
        Cookie cookie = cookieList[0];

        // get token
        String token = cookie.getValue();
        String newToken = JWTUtil.validateToken(token);
        cookie.setValue(newToken);

        // add new cookie to response
        response.addCookie(cookie);

        return HandlerInterceptor.super.preHandle(request, response, handler);
    }
}
