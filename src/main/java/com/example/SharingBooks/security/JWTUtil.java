package com.example.SharingBooks.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;

import static com.example.SharingBooks.model.constants.SecurityConstant.EXPIRATION_TIME;
import static com.example.SharingBooks.model.constants.SecurityConstant.SECRET;

public class JWTUtil {

    public static String generateToken(String subject) {
        return JWT.create()
                .withSubject(subject)
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .sign(Algorithm.HMAC512(SECRET.getBytes()));
    }

    public static String getSubject(String token) {
        DecodedJWT jwt = decode(token);
        return jwt.getSubject();
    }

    public static DecodedJWT decode(String token) {
        return JWT.decode(token);
    }

    public static String validateToken(String token) {
        Date current = new Date(System.currentTimeMillis());
        DecodedJWT jwt = decode(token);

        System.out.println("Current time: " + current.toString());
        System.out.println("Expiration date: " + jwt.getExpiresAt().toString());

        //expired
        if (current.before(jwt.getExpiresAt())) {
            token = generateToken(jwt.getSubject());
        }
        return token;
    }

    public static Date getExpirationDate (String token){
        DecodedJWT jwt = decode(token);
        return jwt.getExpiresAt();
    }
}
