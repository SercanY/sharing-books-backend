package com.example.SharingBooks.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
	
	 public static SessionFactory factory;
	//to disallow creating objects by other classes.

	    private HibernateUtil() {
	    }
	
	public static synchronized SessionFactory getSessionFactory() {

        if (factory == null) {
            factory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
        }
        return factory;
    }

}
