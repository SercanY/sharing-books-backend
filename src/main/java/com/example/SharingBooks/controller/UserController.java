package com.example.SharingBooks.controller;

import com.example.SharingBooks.model.User;
import com.example.SharingBooks.model.constants.CustomHTTPStatus;
import com.example.SharingBooks.model.dto.SBResponseTemplate;
import com.example.SharingBooks.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

import static java.util.Collections.emptyList;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/v1/")
public class UserController implements UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @GetMapping("users")
    public SBResponseTemplate<List<User>> getAllUsers() {
        return new SBResponseTemplate.Builder<List<User>>()
                .code(CustomHTTPStatus.RETURN_ALL_USERS)
                .message("Returned all users")
                .body(userRepository.getAllUser())
                .build();
    }

    @Transactional(rollbackOn = Exception.class)
    @PostMapping("registration")
    public SBResponseTemplate<String> registerValidation(@RequestBody User _user) {
        User user = userRepository.getUserByEmail(_user.getEmail());

        if (user != null) {
            return new SBResponseTemplate.Builder<String>()
                    .code(CustomHTTPStatus.EMAIL_EXISTS)
                    .message("Email already exists")
                    .body("EMAIL ALREADY EXIST")
                    .build();

        }
        _user.setPassword(bCryptPasswordEncoder.encode(_user.getPassword()));
        userRepository.addUser(_user);

        return new SBResponseTemplate.Builder<String>()
                .code(CustomHTTPStatus.USER_SAVED)
                .message("User Saved")
                .body("")
                .build();

    }

    @GetMapping("me")
    public SBResponseTemplate findMe(String email){
        return new SBResponseTemplate.Builder<User>()
                .code(CustomHTTPStatus.USER_FOUND)
                .message("FOUND ME!")
                .body(userRepository.getUserByEmail(email))
                .build();
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        User user = userRepository.getUserByEmail(s);
        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), emptyList());
    }
}