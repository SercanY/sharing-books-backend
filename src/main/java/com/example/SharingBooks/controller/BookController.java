package com.example.SharingBooks.controller;

import com.example.SharingBooks.model.Book;
import com.example.SharingBooks.model.constants.CustomHTTPStatus;
import com.example.SharingBooks.model.dto.BookDto;
import com.example.SharingBooks.model.dto.SBResponseTemplate;
import com.example.SharingBooks.repository.BookRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "localhost:3000")
@RestController
@RequestMapping("/api/v1/book/")
public class BookController {
    private final BookRepository bookRepository;

    public BookController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @GetMapping("all")
    public SBResponseTemplate<List<BookDto>> getAllBooks(){
        return new SBResponseTemplate.Builder<List<BookDto>>()
                .code(CustomHTTPStatus.RETURN_ALL_BOOKS)
                .message("Returned all books")
                .body(bookRepository.getAllBooks())
                .build();
    }
    @GetMapping("myBooks")
    public SBResponseTemplate<List<BookDto>> getMyBooks(){
        /* get user email from token and use below */
        return new SBResponseTemplate.Builder<List<BookDto>>()
                .code(CustomHTTPStatus.RETURN_MY_BOOKS)
                .message("Returned my books")
                .body(null /*bookRepository.getMyBooks(/*  email here */)
                .build();

    }
    @PostMapping("save")
    public SBResponseTemplate<String> saveBook(BookDto bookDto){
        // search for existing
        BookDto searchedBook = bookRepository.searchBook(bookDto.getName());
        if(searchedBook != null){
            return new SBResponseTemplate.Builder<String>()
                    .code(CustomHTTPStatus.BOOK_ALREADY_EXIST)
                    .message("Book already exist!")
                    .body(null)
                    .build();
        }

        bookRepository.addBook(bookDto);
        return new SBResponseTemplate.Builder<String>()
                .code(CustomHTTPStatus.BOOK_SAVED)
                .message("Book SAVED")
                .body(null)
                .build();

    }

    @PostMapping("delete")
    public SBResponseTemplate<String> deleteBook(BookDto bookDto){
        BookDto searchedBook = bookRepository.searchBook(bookDto.getName());
        if (searchedBook == null){
            return new SBResponseTemplate.Builder<String>()
                    .code(CustomHTTPStatus.BOOK_NOT_FOUND)
                    .message("Book not found!")
                    .body("")
                    .build();
        }

        deleteBook(searchedBook);
        return new SBResponseTemplate.Builder<String>()
                .code(CustomHTTPStatus.DELETION_SUCCESS)
                .message("Book deleted")
                .body("")
                .build();
    }


}
