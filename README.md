# Shared Book

This is a project about sharing books between people. In this project, I am responsible for implementing web sites for people who want to share book already read by owner. People are able to trade their books in exchange of their own book.

Tools and libraries used for this project:
-JAVA 15
-Spring BOOT
-Hibernate
-PostgreSQL
-React Hook
-NextJS
-Redux
